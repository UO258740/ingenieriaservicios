import socket, sys

# Creacion del socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

if len(sys.argv)<3:
        puerto = 9999
        print("puerto por defecto: ", puerto)
        if len(sys.argv)<2:
                direccion = "localhost"
                print("direccion por defecto: ", direccion)
        else:
                direccion = sys.argv
                print("direccion: ", direccion)
else:
        direccion = sys.argv[1]
        puerto = sys.argv[2]
        print("direccion: ", direccion)
        print("puerto: ", puerto)

# Conectar el socket
s.connect((direccion, puerto))

cont = 1
mensaje = "ABCDE"
while cont<=5:
        datagrama = s.sendall(mensaje.encode("utf8"))
        cont += 1
s.send(b"FINAL")
s.close()

