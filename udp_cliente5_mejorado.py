import socket, sys

if len(sys.argv)<3:
        puerto = 9999
        print("puerto por defecto: ", puerto)
        if len(sys.argv)<2:
                direccion = "localhost"
                print("direccion por defecto: ", direccion)
        else:
                direccion = sys.argv[1]
                print("direccion: ", direccion)
else:
        direccion = sys.argv[1]
        puerto  = sys.argv[2]
        print("direccion: ", direccion)
        print("puerto: ", puerto)


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
tiempo = 0.5
s.settimeout(tiempo)
cont = 1
texto = input("Texto: ")
while texto!="FIN":
        texto = str(cont) + ": " + texto
        print(texto)
        textoC = texto.encode("utf8")
        datagrama = s.sendto(textoC, (direccion, puerto))
        try:
               s.connect((direccion, puerto))
               datagrama, origen = s.recv(1024)
               datagrama = datagrama.decode("utf8")
               cont+=1
               texto = input("Texto: ")
               if datagrama=="OK":
                        print("Recibida confirmación")
               else:
                        print("Recibido datagrama no esperado")
        except socket.timeout:
               if tiempo<=2:
                        s.sendto(textoC, (direccion, puerto))
                        tiempo = tiempo*2
                        s.settimeout(tiempo)
               else:
                        print("Puede que el servidor este caido. Intentelo mas tarde")
                        exit()

