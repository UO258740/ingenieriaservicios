import socket, sys, random, hashlib

if len(sys.argv)<2:
        puerto = 9999
        print("Puerto por defecto: ", puerto)
else:
        puerto = sys.argv[1]
        print("Puerto: ", puerto)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("", puerto))
lista = []
print("Escuchando...")
while True:
        datagrama, origen = s.recvfrom(1024)
        prob = random.randint(1, 100)
        datagramaD = datagrama.decode("utf8")
        if prob>50:
                print("Simulando paquete perdido")
        else:
                if datagramaD in lista:
                        respuesta = b"Datagrama repetido"
                else:
                        lista.append(datagrama.decode("utf8"))
                        respuesta = b"OK"
                print("Direccion del datagrama:  ", origen)
                print("Contenido del datagrama: ", datagrama.decode("utf8"))
                #s.sendto(datagrama, origen)
                s.sendto(respuesta, origen)

