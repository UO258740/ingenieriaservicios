import socket, sys

def recibe_mensaje(socket):
        byte = ""
        buffer = []

        # Conversion de socket a fichero
        fichero = socket.makefile(encoding="utf8", newline="\r\n")

        # Creacion del mensaje
        mensaje = fichero.readline()
        return mensaje

# Creacion del socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

if len(sys.argv)<3:
        puerto = 9999
        print("puerto por defecto: ", puerto)
        if len(sys.argv)<2:
                direccion = "localhost"
                print("direccion por defecto: ", direccion)
        else:
                direccion = sys.argv
                print("direccion: ", direccion)
else:
        direccion = sys.argv[1]
        puerto = sys.argv[2]
        print("direccion: ", direccion)
        print("puerto: ", puerto)

# Conectar el socket
s.connect((direccion, puerto))

s.send(b"Primer mensaje\r\n")
datagrama = recibe_mensaje(s)
print(datagrama)

s.send(b"Segundo mensaje\r\n")
datagrama = recibe_mensaje(s)
print(datagrama)

s.send(b"Tercer mensaje\r\n")
datagrama = recibe_mensaje(s)
print(datagrama)

