import getpass
import telnetlib
import time

HOST = "localhost"
#user = input("Enter your remote account: ")
#password = getpass.getpass()
user = "alumno"
password = "rom31abi"

tn = telnetlib.Telnet(HOST)

tn.read_until(b"login: ")
tn.write(user.encode('utf8') + b"\n")
if password:
    tn.read_until(b"Password: ")
    tn.write(password.encode('utf8') + b"\n")

tn.read_until(b"$")
tn.write(b"ps -ef\n")

respuesta = tn.read_until(b"$").decode('utf8')

if "udp_servidor3_con_ok.py" in respuesta:
	print("El servidor ya está en ejecución")
	tn.write(b"exit\n")
else:
	print("Lanzando servidor")
	tn.write(b"nohup python3 udp_servidor3_con_ok.py &\n")
	time.sleep(1)
	tn.write(b"exit\n")

print(tn.read_all().decode('utf8'))

