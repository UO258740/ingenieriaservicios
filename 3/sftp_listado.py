import paramiko
import getpass
import base64
import sys

ip = ""
user = ""

if len(sys.argv)==3:
	user = sys.argv[1]
	ip = sys.argv[2]
else:
	user = 'alumno'
	ip = 'localhost'

# Se crea la conexion SSH con paramiko tal y como se ha realizado
# en los ejercicios anteriores
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.WarningPolicy())
print("User ", user, "\nIP ", ip)
passwd = getpass.getpass("Password: ")

# Se conecta y se establece la conexion
client.connect(ip, username=user, password=passwd)
print("Conexion establecida")

# Se abre el canal stfp
sftp = client.open_sftp()

# Se listan los contenidos de la carpeta del usuario
sftp.chdir()
listado = sftp.listdir()
for nombre in listado:
    print(nombre)


