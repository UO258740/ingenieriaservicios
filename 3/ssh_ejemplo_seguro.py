import paramiko
import getpass
import base64

client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.WarningPolicy())

passwd = getpass.getpass("Password: ")

key = paramiko.Ed25519Key(data=
base64.decodestring(b'AAAAC3NzaC1lZDI1NTE5AAAAIIhp/8xGRXsdlHaZ1tP7GAU9I3qD3DXf6aXonm33k69Q'))
client.get_host_keys().add('localhost', 'ssh-ed25519', key)

client.connect('localhost', username='alumno', password=passwd)
print("Conectado!!")

# Ejecutar comando remoto, redireccionando sus salidas
stdin, stdout, stderr = client.exec_command('ls')

# Mostrar resultado de la ejecución (rstrip quita los retornos de carro)
for line in stdout:
    print(line.rstrip())
client.close()

