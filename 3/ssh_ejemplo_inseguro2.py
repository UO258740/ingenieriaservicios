import paramiko
import getpass

client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.WarningPolicy())

passwd = getpass.getpass("Password: ")
client.connect('localhost', username='alumno', password=passwd)
print("Conectado!!")

# Ejecutar comando remoto, redireccionando sus salidas
stdin, stdout, stderr = client.exec_command('ls')

# Mostrar resultado de la ejecución (rstrip quita los retornos de carro)
for line in stdout:
    print(line.rstrip())
client.close()
