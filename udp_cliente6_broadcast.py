import socket, sys

if len(sys.argv)<3:
        puerto = 12345
        print("puerto por defecto: ", puerto)
        if len(sys.argv)<2:
                direccion = "localhost"
                print("direccion por defecto: ", direccion)
        else:
                direccion = sys.argv[1]
                print("direccion: ", direccion)
else:
        direccion = sys.argv[1]
        puerto = sys.argv[2]
        print("direccion: ", direccion)
        print("puerto: ", puerto)

s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

tiempo = 1

menBroadcast = "BUSCANDO HOLA"
datBroadcast = s.sendto(menBroadcast.encode("utf8"), (direccion, puerto))
print("Enviando mensaje broadcast")

s.settimeout(tiempo)
while True:
        try:
             datagramaServ, origen = s.recvfrom(1024)
             mensajeServ = datagramaServ.decode("utf8")
             if mensajeServ=="IMPLEMENTO HOLA":
                  print("Conectado con servidor")
        except socket.timeout:
                print("No ha respondido ningun servidor")
                exit()

if origen!=None:
        s.sendto(b"HOLA", origen)
        respuestaS, direccionS = s.recvfrom(1024)
        if "HOLA" in respuestaS:
                print("Broadcast correcto")
                print("IP servidor: " + direccionS)
                print("respuesta servidor: " + respuestaS.decode("utf8"))
        else:
                print("Broadcast fallido")
                print(direccionS)
                print(respuestaS.decode("utf8"))

