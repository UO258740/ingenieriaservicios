import socket, sys

#Definicion de la funcion recibe_mensaje()
def recibe_mensaje(socket):
	buffer = []
	byteRecibido=""
	byteR=""
	byteN=""
        #Mientras el byte no forme parte del terminador de linea
	while byteN!=b"\n" or byteR!=b"\r":
                #Se recibe un byte
		byteRecibido = socket.recv(1);
                #ar el bucle y enviar los datos
		if byteRecibido==b"\r":
                        byteR=byteRecibido
		elif byteRecibido==b"\n":
                        byteN=byteRecibido
		elif len(byteRecibido) == 0:
                        return byteRecibido
		buffer.append(byteRecibido)
	return b"".join(buffer)

# Se analizan los parametros
if len(sys.argv)<3:
        puerto = 9999
        print("puerto por defecto: ", puerto)
        if len(sys.argv)<2:
                direccion = "localhost"
                print("direccion por defecto: ", direccion)
        else:
                direccion = sys.argv
                print("direccion: ", direccion)
else:
        direccion = sys.argv[1]
        puerto = sys.argv[2]
        print("direccion: ", direccion)
        print("puerto: ", puerto)

# Se crea y se conecta el socket
sd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sd.connect((direccion,puerto))

sd.send(b"Primer mensaje\r\n")
print(recibe_mensaje(sd).decode("utf-8"))
sd.send(b"Segundo mensaje\r\n")
print(recibe_mensaje(sd).decode("utf-8"))
sd.send(b"Tercer mensaje\r\n")
print(recibe_mensaje(sd).decode("utf-8"))

