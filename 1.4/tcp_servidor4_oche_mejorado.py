import socket , time, sys
#Se define de la funcion recibe_mensaje()
def recibe_mensaje(socket):
	buffer = []
	byteRecibido=""
	byteR=""
	byteN=""
	#Mientras el byte no forme parte del terminador de linea
	while byteN!=b"\n" or byteR!=b"\r":
		#Se recibe un byte
		byteRecibido = socket.recv(1);
		#Se evalua el byte recibido,
		#usando dos variables para las marcas de fin de linea
		if byteRecibido==b"\r":
			byteR=byteRecibido
		elif byteRecibido==b"\n":
			byteN=byteRecibido
		elif len(byteRecibido) == 0:
			return byteRecibido
		buffer.append(byteRecibido)
	return b"".join(buffer)

# Se crea del socket de escucha
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#Se comprueba si el puerto esta introducido correctamente
if len(sys.argv) < 2:
	puerto = 9999
	print("puerto por defecto: ", puerto)
else:
	puerto = int(sys.argv[1], 10)
	print("puerto: ", puerto)
# Se asigna el puerto
s.bind(("", puerto))
print("Puerto asginado: ", puerto)
# Se pone el socket en modo pasivo
s.listen(5)  # Máximo de clientes en la cola de espera al accept()
# Bucle principal de espera por clientes
while True:
	print("Esperando un cliente")
	sd, origen = s.accept()
	time.sleep(1)
	print("Nuevo cliente conectado desde %s, %d" % origen)
	continuar = True
	# Bucle de atención al cliente conectado
	while continuar:
		# Primero recibir el mensaje del cliente
		#mensaje = sd.recv(80) # Nunca enviará más de 80 bytes, aunque tal vez sí menos
		mensaje = recibe_mensaje(sd)
		mensaje = str(mensaje, "utf8") # Convertir los bytes a utf-8
		# Segundo, quitarle el "fin de línea" que son sus 2 últimos caracteres
		# caracteres
		linea = mensaje[:-2] # slice desde el principio hasta el final -2
		# Tercero, darle la vuelta
		linea = linea[::-1]
		sd.sendall(bytes(linea+"\r\n","utf8"))
		if mensaje =="":
			print("El cliente ha cerrado la conexion")
			sd.close()
			continuar=False
		elif mensaje=="FINAL\r\n":
			print("El cliente ha finalizado la conexion")
			sd.close()
			continuar=False
		else:
			print("mensaje: ", mensaje)

