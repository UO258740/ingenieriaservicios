import socket, sys

def recibe_mensaje(socket):
	# Suponemos que sd es un socket previamente inicializado y conectado
	f = sd.makefile(encoding="utf8", newline="\r\n")
	# Ahora ya se puede
	mensaje = f.readline()   # Lee bytes hasta detectar \r\n
	# El mensaje retornado es un str, y contiene \r\n al final
	return mensaje

def recibe_longitud(socket):
	buffer = []
	byte = b""
	while byte!=b"\n":
		byte = socket.recv(1)
		buffer.append(byte)
	return b"".join(buffer)

def recv_resto_mensaje(socket, longitud):
	mensaje = socket.recv(longitud)
	return mensaje

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
if len(sys.argv)<2:
	puerto = 9999
	print("puerto por defecto: ", puerto)
else:
	puerto = sys.argv[1]
	print("puerto: ", puerto)
s.bind(("", puerto))
s.listen(5)
# Bucle principal de espera por clientes
while True:
	print("Esperando un cliente")
	sd, origen = s.accept()
	print("Nuevo cliente conectado desde %s, %d" % origen)
	continuar = True
# Bucle de atención al cliente conectado
	while continuar:
		# Primero recibir el mensaje del cliente
		longitud = recibe_longitud(sd)
		longitud = str(longitud, "utf8")
		#mensaje = sd.recv(80)   Nunca enviará más de 80 bytes, aunque tal vez sí menos
		mensaje = recv_resto_mensaje(sd, int(longitud))
		mensaje = str(mensaje, "utf8") # Convertir los bytes a caracteres
		# Segundo, quitarle el "fin de línea" que son sus 2 últimos caracteres
		linea = mensaje[:-2]  # slice desde el principio hasta el final -2
		# Tercero, darle la vuelta
		linea = linea[::-1]
		# Finalmente, enviarle la respuesta con un fin de línea añadido
		# Observa la transformación en bytes para enviarlo
		sd.sendall(bytes(linea+"\r\n", "utf8"))
		if mensaje=="":
			print("El cliente ha cerrado la conexion")
			sd.close()
		elif mensaje=="FINAL":
			print("El cliente ha finalizado la conexion")
			sd.close()
			continuar = False
		else:
			print("mensaje: ", mensaje)
s.close()


