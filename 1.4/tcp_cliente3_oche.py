import socket, sys

# Creacion del socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

if len(sys.argv)<3:
        puerto = 9999
        print("puerto por defecto: ", puerto)
        if len(sys.argv)<2:
                direccion = "localhost"
                print("direccion por defecto: ", direccion)
        else:
                direccion = sys.argv
                print("direccion: ", direccion)
else:
        direccion = sys.argv[1]
        puerto = sys.argv[2]
        print("direccion: ", direccion)
        print("puerto: ", puerto)

# Conectar el socket
s.connect((direccion, puerto))
# Envía el primer mensaje
s.send(b"Primer mensaje\r\n")
datagrama = s.recv(1024).decode("utf8")
print(datagrama)
# Envía el segundo mensaje
s.send(b"Segundo mensaje\r\n")
datagrama = s.recv(1024).decode("utf8")
print(datagrama)
# Envía el tercer mensaje
s.send(b"Tercer mensaje\r\n")
datagrama = s.recv(1024).decode("utf8")
print(datagrama)


