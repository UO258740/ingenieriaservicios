import socket, sys

def recibe_mensaje(socket):
        byte = ""
        buffer = []
        # Conversion de socket a fichero
        fichero = socket.makefile(encoding="utf8", newline="\r\n")
        # Creacion del mensaje
        mensaje = fichero.readline()
        return mensaje

# Creacion del socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
if len(sys.argv)<3:
        puerto = 9999
        print("puerto por defecto: ", puerto)
        if len(sys.argv)<2:
                direccion = "localhost"
                print("direccion por defecto: ", direccion)
        else:
                direccion = sys.argv
                print("direccion: ", direccion)
else:
        direccion = sys.argv[1]
        puerto = sys.argv[2]
        print("direccion: ", direccion)
        print("puerto: ", puerto)

# Conectar el socket
s.connect((direccion, puerto))
mensaje = "Primer mensaje"
longitud  = "%d\n" % len(bytes(mensaje, "utf8"))  # Pasamos a ASCII la longitud
s.sendall(bytes(longitud + mensaje, "utf8"))      # e incluimos el delimitador
datagrama = s.recv(1024).decode("utf8")		  # Enviamos la concatenación
print("Mensaje: ", datagrama)

mensaje = "Segundo mensaje"
longitud  = "%d\n" % len(bytes(mensaje, "utf8"))  # Pasamos a ASCII la longitud
s.sendall(bytes(longitud + mensaje, "utf8"))      # e incluimos el delimitador
datagrama = s.recv(1024).decode("utf8")		  # Enviamos la concatenación
print("Mensaje: ", datagrama)

mensaje = "Tercer mensaje"
longitud  = "%d\n" % len(bytes(mensaje, "utf8"))  # Pasamos a ASCII la longitud
s.sendall(bytes(longitud + mensaje, "utf8"))      # e incluimos el delimitador
datagrama = s.recv(1024).decode("utf8")		  # Enviamos la concatenación
print("Mensaje: ", datagrama)

mensaje = "FINAL"
longitud  = "%d\n" % len(bytes(mensaje, "utf8"))  # Pasamos a ASCII la longitud
s.sendall(bytes(longitud + mensaje, "utf8")) 	  # e incluimos el delimitador
datagrama = s.recv(1024).decode("utf8")           # Enviamos la concatenación

