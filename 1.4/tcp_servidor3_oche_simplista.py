import socket, sys, time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
if len(sys.argv)<2:
        puerto = 9999
        print("puerto por defecto: ", puerto)
else:
        puerto = sys.argv[1]
        print("puerto: ", puerto)

s.bind(("localhost", puerto))
s.listen(5)
# Bucle principal de espera por clientes
while True:
    print("Esperando un cliente")
    sd, origen = s.accept()
    time.sleep(1)
    print("Nuevo cliente conectado desde %s, %d" % origen)
    continuar = True
 # Bucle de atención al cliente conectado
    while continuar:
        # Primero recibir el mensaje del cliente
        mensaje = sd.recv(80)  # Nunca enviará más de 80 bytes, aunque tal vez sí menos
        mensaje = str(mensaje, "utf8") # Convertir los bytes a caracteres
        # Segundo, quitarle el "fin de línea" que son sus 2 últimos caracteres
        linea = mensaje[:-2]  # slice desde el principio hasta el final -2
        # Tercero, darle la vuelta
        linea = linea[::-1]

        # Finalmente, enviarle la respuesta con un fin de línea añadido
        # Observa la transformación en bytes para enviarlo
        sd.sendall(bytes(linea+"\r\n", "utf8"))

        if mensaje=="":
                print("El cliente ha cerrado la conexion")
                sd.close()
        elif mensaje=="FINAL\r\n":
                print("El cliente ha finalizado la conexion")
                sd.close()
                continuar = False
        else:
                print("mensaje: ", mensaje)
s.close()

