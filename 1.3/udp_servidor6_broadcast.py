import socket, sys, random

if len(sys.argv)<2:
	puerto = 12345
	print("Puerto por defecto: ", puerto)
else:
	puerto = sys.argv[1]
	print("Puerto: ", puerto)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("", puerto))
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
print("Escuchando...")
while True:
	datagrama, origen = s.recvfrom(1024)
	mensaje = datagrama.decode("utf8")
#	prob = random.randint(1, 100)
#	if prob>50:
#		print("Simulando paquete perdido")
#	else:
	if mensaje=="BUSCANDO HOLA":
		s.sendto(b"IMPLEMENTO HOLA",origen)
	else:
		if mensaje=="HOLA":
			datagramaResp = "HOLA: " + origen[0]
			s.sendto(datagramaResp.encode("utf8"), origen)
	print("Direccion del datagrama:  ", origen[0])
	print("Contenido del datagrama: ", mensaje)
	# s.sendto(datagrama, origen)
	# datagramaResp = b"OK"

