import socket, sys

if len(sys.argv)<3:
	puerto = 12345
	print("puerto por defecto: ", puerto)
	if len(sys.argv)<2:
		direccion = "localhost"
		print("direccion por defecto: ", direccion)
	else:
		direccion = sys.argv[1]
		print("direccion: ", direccion)
else:
	direccion = sys.argv[1]
	puerto = sys.argv[2]
	print("direccion: ", direccion)
	print("puerto: ", puerto)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
direccionServ = None
tiempo = 2

menBroadcast = "BUSCANDO HOLA"
datBroadcast = s.sendto(menBroadcast.encode("utf8"), (direccion, puerto))
print("Enviando mensaje broadcast")

s.settimeout(tiempo)
while True:
	try:
		datagramaServ, origen = s.recvfrom(1024)
		mensajeServ = datagramaServ.decode("utf8")
		if mensajeServ=="IMPLEMENTO HOLA":
			print("Conectado con servidor")
			if direccionServ == None:
				direccionServ = origen
				print("Direccion del servidor: ", str(direccionServ))
			else:
				print("Direccion del servidor: ", str(direccionServ))
	except socket.timeout:
		print("No hay mas respuestas")
		break

if direccionServ == None:
	print("No ha habido respuesta")
	exit()

s.sendto(b"HOLA", direccionServ)

#if origen != None:
s.sendto(b"HOLA", origen)
respuestaS, direccionS = s.recvfrom(1024)
if b"HOLA" in respuestaS:
	print("Broadcast correcto")
	#print("IP servidor: " + str(direccionS))
	print("respuesta servidor: " + respuestaS.decode("utf8"))
	sys.exit()
else:
	print("Broadcast fallido")
	#print(str(direccionS))
	print(respuestaS.decode("utf8"))
	sys.exit()
