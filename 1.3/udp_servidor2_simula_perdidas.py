import socket, sys, random

if len(sys.argv)<2:
        puerto = 9999
        print("Puerto por defecto: ", puerto)
else:
        puerto = sys.argv[1]
        print("Puerto: ", puerto)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("", puerto))
print("Escuchando...")
while True:
        datagrama, origen = s.recvfrom(1024)
        prob = random.randint(1, 100)
        if prob>50:
                print("Simulando paquete perdido")
        else:
                print("Direccion del datagrama:  ", origen)
                print("Contenido del datagrama: ", datagrama.decode("utf8"))
                s.sendto(datagrama, origen)


