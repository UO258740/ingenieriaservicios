import socket, sys

if len(sys.argv)<3:
	puerto = 9999
	print("puerto por defecto: ", puerto)
	if len(sys.argv)<2:
		direccion = "localhost"
		print("direccion por defecto: ", direccion)
	else:
		direccion = sys.argv[1]
		print("direccion: ", direccion)
else:
	direccion = sys.argv[1]
	puerto  = sys.argv[2]
	print("direccion: ", direccion)
	print("puerto: ", puerto)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect((direccion, puerto))
s.settimeout(0.5)
cont = 1
texto = str(cont) + ": " + input("Texto: ")
while texto!="FIN":
	print(texto)
	textoC = texto.encode("utf8")
	datagrama = s.sendto(textoC, (direccion, puerto))
	try:
		datagrama, origen = s.recvfrom(1024)
		datagrama = datagrama.decode("utf8")
		if datagrama=="OK":
			print("Recibida confirmación")
		else:
			print("Recibido datagrama no esperado")
	except socket.timeout:
		print("ERROR. El datagrama de confirmación no llega")
	cont+=1
	texto = str(cont) + ":" + input("Texto: ")

