import socket, sys

if len(sys.argv)<3:
        puerto = 9999
        print("puerto por defecto: ", puerto)
        if len(sys.argv)<2:
                direccion = "localhost"
                print("direccion por defecto: ", direccion)
        else:
                direccion = sys.argv[1]
                print("direccion: ", direccion)
else:
        direccion = sys.argv[1]
        puerto  = sys.argv[2]
        print("direccion: ", direccion)
        print("puerto: ", puerto)


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect((direccion, puerto))
cont = 1
texto = input("Texto: ")
while texto!="FIN":
        texto = str(cont) + ": " + texto
        print(texto)
        textoC = texto.encode("utf8")
        datagrama = s.sendto(textoC, (direccion, puerto))
        cont+=1
        texto = input("Texto: ")

