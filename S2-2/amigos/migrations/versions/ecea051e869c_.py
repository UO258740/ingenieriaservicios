"""empty message

Revision ID: ecea051e869c
Revises: 
Create Date: 2021-01-10 22:05:35.008254

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ecea051e869c'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('amigos',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=32), nullable=True),
    sa.Column('longi', sa.String(length=32), nullable=True),
    sa.Column('lati', sa.String(length=32), nullable=True),
    sa.Column('device', sa.Text(length=4096), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('amigos')
    # ### end Alembic commands ###
