import socket, sys, random

if len(sys.argv)<2:
        puerto = 12345
        print("Puerto por defecto: ", puerto)
else:
        puerto = sys.argv[1]
        print("Puerto: ", puerto)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("", puerto))
s.setsockopt(s.SOL_SOCKET, s.SO_BROADCAST, 1)
print("Escuchando...")
while True:
        datagrama, origen = s.recvfrom(1024)
        mensaje = datagrama.decode("utf8")
        prob = random.randint(1, 100)
        if prob>50:
                print("Simulando paquete perdido")
        else:
                if mensaje=="BUSCANDO HOLA":
                        datagramaResp = b"IMPLEMENTO HOLA" + origen[0]
                else if mensaje=="HOLA":
                        datagramaResp = b"HOLA: " + origen[0]
                print("Direccion del datagrama:  ", origen[0])
                print("Contenido del datagrama: ", mensaje)
                # s.sendto(datagrama, origen)
                # datagramaResp = b"OK"
                s.sendto(datagramaResp, origen)

