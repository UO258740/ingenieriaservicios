from sleekxmpp import ClientXMPP
import logging
import getpass

class MiBot(ClientXMPP): # Esta es la sintaxis de la herencia en python
#     definición del constructor:
#        llamar al constructor de la clase base
#        registrar el callback para el evento "session_start"
#        registrar el callback para el evento "message"
	def __init__(self, jid, clave):
		super().__init__(jid, clave)
		self.add_event_handler("session_start", self.callback_para_session_start)
		self.add_event_handler("message", self.callback_para_message)

#    definicion del callback para el evento "session_start":
#        enviar stanza de presencia "online"
#        enviar stanza de petición del roster
	def callback_para_session_start(self, evento):
		self.send_presence()
		self.get_roster()

#    definicion del callback para el evento "message":
#        si el mensaje es de tipo "chat":
#            obtener el origen del mensaje
#            obtener el body del mensaje
#            componer el mensaje "¿%s?" % body
#            enviar al origen el mensaje que hemos compuesto
	def callback_para_message(self, evento):
		print("Recibido un mensaje de tipo %s desde %s" %
			(evento["type"], evento["from"]))
		print("Que dice: %s" % evento["body"])
		if evento["type"]=="chat":
			if evento["body"][0] == "=":
				expresion = evento["body"][1:]
				print("Expresión a analizar: ", expresion)
				resultado = eval(expresion)
				print("Resultado: ", resultado)
			msg = self.Message() #     Crear mensaje vacío
			# Añadir campos
			msg["to"] = jid_destino #  es una cadena
			msg["type"] = "chat" #     (en este caso)
			msg["body"] = cuerpo #     es otra cadena
			msg["chat_state"] = "active"
			msg.send() #               Envío

#   definicion de los callbacks para los eventos de deteccion de estados del bot
	def callback_para_activo(self, evento):
		print(evento["from"].bare + " está activo")

	def callback_para_escribiendo(self, evento):
		print(evento["from"].bare + " está escribiendo...")

	def callback_para_parar_escribir(self, evento):
		print(evento["from"].bare + " ha parado de escribir")

	def callback_para_inactivo(self, evento):
		print(evento["from"].bare + " está inactivo")

# Programa principal
usuario = "bot"
recurso = "bot"
# Pedir al usuario su JID
jid = "bot@ingserv123"
print("Usuario ", jid)
# Pedir al usuario su contraseña
clave = getpass.getpass("Contraseña: ")
# Pedir al usuario la IP del servidor
ip = "localhost"
puerto = 5222

# Instanciar el cliente, con el jid y clave del bot
cliente = MiBot(jid, clave)
cliente.register_plugin("xep_0085")  # chatstates

# Depuracion de mensajes
logging.basicConfig(level=logging.INFO,
                format='%(levelname)-8s %(message)s')

# Conectar con la IP del servidor. El puerto estándar es el 5222
cliente.connect((ip, puerto))

# Iniciar el bucle de eventos
cliente.process()
