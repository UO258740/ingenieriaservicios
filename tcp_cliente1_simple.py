import socket
import sys

# Se crea el socket tcp
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Se piden la IP y el puerto
ip = input("Introduce la direccion IP (enter por defecto): ")
puerto = input("Introduce el puerto (entre por defecto): ")
ip = "localhost" # if (len(ip)==0) 
puerto = 9999 # if (len(puerto)==0) 

print("IP escogida " + ip)
print("puerto escogido " + str(puerto))

# Direccion del puerto
direccion = (ip, puerto)

# Conecta el socket con el servidor
s.connect(direccion)

cont = 0
while cont<5:
    cadena = "ABCDE"
    print("enviando cadena " + cadena)
    s.send(bytes(cadena, "ascii"))
    cont = cont+1

print("enviando FINAL")
s.send("FINAL")
s.close()